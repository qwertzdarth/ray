use std::option::Option;

use crate::hittable::HitRecord;
use crate::ray::Ray;
use crate::color::Color;
use crate::vec3::Vec3;
use super::Material;
use super::ScatterInfo;

pub struct Diffuse {
    pub albedo: Color,
}

impl Diffuse {
    pub fn create(albedo: Color) -> Diffuse {
        Diffuse { albedo }
    }
}

impl Material for Diffuse {
    fn scatter(
        &self,
        _ray: &Ray,
        hit_record: &HitRecord,
    ) -> Option<ScatterInfo> {
        let mut scatter_direction = hit_record.normal + Vec3::random_on_unit_sphere();
        
        if scatter_direction.near_zero() {
            scatter_direction = hit_record.normal;
        }

        Some(ScatterInfo {
            attenuation: self.albedo,
            scattered: Ray { origin: hit_record.p, direction: scatter_direction },
        })
    }
}
