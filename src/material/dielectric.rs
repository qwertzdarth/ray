use rand::Rng;

use super::*;

use crate::color::Color;
use crate::hittable::HitRecord;
use crate::ray::Ray;

pub struct Dielectric {
    ir: f32,
}

impl Dielectric {
    pub fn create(ir: f32) -> Self {
	Self { ir }
    }
}

fn reflectance(cosine: f32, ref_idx: f32) -> f32 {
    let mut r0 = (1.0 - ref_idx) / (1.0 + ref_idx);
    r0 = r0 * r0;
    r0 + (1.0 - r0) * (1.0 - cosine).powi(5)
}

impl Material for Dielectric {
    fn scatter(
        &self,
        ray: &Ray,
        hit_record: &HitRecord,
    ) -> Option<ScatterInfo> {
	let mut rng = rand::thread_rng();
	
	let refraction_ratio = if hit_record.front_face { 1.0 / self.ir } else { self.ir };

	let unit_direction = ray.direction.normalized();
	
	let cos_theta = (-unit_direction).dot(hit_record.normal).min(1.0);
	let sin_theta = (1.0 - cos_theta * cos_theta).sqrt();

	if refraction_ratio * sin_theta <= 1.0 && reflectance(cos_theta, refraction_ratio) <= rng.gen_range(0.0..1.0) {
	    Some(ScatterInfo {
		attenuation: Color::WHITE,
		scattered: Ray {
		    origin: hit_record.p,
		    direction: refract(unit_direction, hit_record.normal, refraction_ratio)
		},
	    })
	} else {
	    Some(ScatterInfo {
		attenuation: Color::WHITE,
		scattered: Ray {
		    origin: hit_record.p,
		    direction: reflect(unit_direction, hit_record.normal),
		}
	    })
	}
    }
}
