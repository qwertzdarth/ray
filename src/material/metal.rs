use crate::hittable::HitRecord;
use crate::ray::Ray;
use crate::color::Color;
use crate::vec3::Vec3;
use super::*;

pub struct Metal {
    pub albedo: Color,
    pub fuzz: f32,
}

impl Metal {
    pub fn create(albedo: Color, fuzz: f32) -> Metal {
        Metal { 
            albedo,
            fuzz: if fuzz <= 1.0 { fuzz } else { 1.0 }
        }
    }
}

impl Material for Metal {
    fn scatter(
        &self,
        ray: &Ray,
        hit_record: &HitRecord,
    ) -> Option<ScatterInfo> {
        let reflected = reflect(ray.direction.normalized(), hit_record.normal);

        if reflected.dot(hit_record.normal) > 0.0 {
            Some(ScatterInfo {
                attenuation: self.albedo,
                scattered: Ray { origin: hit_record.p, direction: reflected + self.fuzz * Vec3::random_in_unit_sphere() }
            })
        } else {
            None
        }
    }
}
