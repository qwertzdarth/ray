use std::option::Option;

use crate::hittable::HitRecord;
use crate::ray::Ray;
use crate::color::Color;
use crate::vec3::Vec3;

pub use metal::*;
pub use diffuse::*;
pub use dielectric::*;
pub use diffuse_light::*;

pub mod metal;
pub mod diffuse;
pub mod dielectric;
pub mod diffuse_light;

pub struct ScatterInfo {
    pub attenuation: Color,
    pub scattered: Ray,
}

pub trait Material {
    fn scatter(
        &self,
        ray: &Ray,
        hit_record: &HitRecord,
    ) -> Option<ScatterInfo>;

    fn emitted(&self) -> Color {
	Color::BLACK
    }
}

fn refract(v: Vec3, normal: Vec3, refraction_ratio: f32) -> Vec3 {
    let cos_theta = (-v).dot(normal).min(1.0);
    let r_perpendicular = refraction_ratio * (v + cos_theta * normal);
    let r_parallel = -((1.0 - r_perpendicular.length_squared()).abs()).sqrt() * normal;

    r_perpendicular + r_parallel
}


fn reflect(v: Vec3, n: Vec3) -> Vec3 {
    v - 2.0 * v.dot(n) * n
}
