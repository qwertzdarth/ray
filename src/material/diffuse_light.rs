
use std::option::Option;

use crate::*;

pub struct DiffuseLight {
    pub albedo: Color,
}

impl DiffuseLight {
    pub fn create(albedo: Color) -> Self {
        Self { albedo }
    }
}

impl Material for DiffuseLight {
    fn scatter(
        &self,
        _ray: &Ray,
        _hit_record: &HitRecord,
    ) -> Option<ScatterInfo> {
	None
    }

    fn emitted(&self) -> Color {
	self.albedo
    }
}
