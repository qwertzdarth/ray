use crate::*;

use std::sync::Arc;
use std::option::Option;

pub struct Sphere {
    pub center: Vec3,
    pub radius: f32,
    pub material: Arc<dyn Material + Send + Sync>,
}

impl Hittable for Sphere {
    fn hit(&self, r: Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
        let oc = r.origin - self.center;
        let a = r.direction.length_squared();
        let half_b = oc.dot(r.direction);
        let c = oc.length_squared() - self.radius * self.radius;

        let discriminant = half_b * half_b - a * c;
        let sqrtd = discriminant.sqrt();
        if discriminant < 0.0 {
            Option::None
        } else {
            let root1 = (-half_b - sqrtd) / a;
            let root2 = (-half_b + sqrtd) / a;
            if t_min <= root1 && root1 <= t_max {
                let p = r.at(root1);
                Option::Some(HitRecord::create_with_outward_normal(r, p, (p - self.center) / self.radius, root1, self.material.clone()))
            } else if t_min <= root2 && root2 <= t_max {
                let p = r.at(root2);
                Option::Some(HitRecord::create_with_outward_normal(r, p, (p - self.center) / self.radius, root2, self.material.clone()))
            } else {
                Option::None
            }
        }
    }

    fn bounding_box(&self) -> Option<AABB> {
	let r = Vec3::create(self.radius, self.radius, self.radius);
	
	Some(AABB::create(self.center - r, self.center + r))
    }
}
