use crate::vec3::Vec3;
use std::ops;

use rand::Rng;

#[derive(Debug, Copy, Clone)]
pub struct Color {
    pub r: f32,
    pub g: f32,
    pub b: f32,
}

enum ColorComponent {
    Red,
    Green,
    Blue,
}

fn clamp(x: f32, min: f32, max: f32) -> f32 {
    if x < min {
        min
    } else if x > max {
        max
    } else {
        x
    }
}

impl Color {
    pub const BLACK: Color = Color {
        r: 0.0,
        g: 0.0,
        b: 0.0,
    };

    pub const WHITE: Color = Color {
        r: 1.0,
        g: 1.0,
        b: 1.0,
    };

    pub const LIGHT_BLUE: Color = Color {
        r: 0.5,
        g: 0.7,
        b: 1.0,
    };

    pub const SKY_BLUE: Color = Color {
        r: 0.4,
        g: 0.6,
        b: 1.0,
    };

    pub const RED: Color = Color {
        r: 1.0,
        g: 0.3,
        b: 0.3,
    };

    pub fn create(r: f32, g: f32, b: f32) -> Color {
        Color { r, g, b }
    }

    pub fn from_vec(v: Vec3) -> Color {
        Color {
            r: v.x(),
            g: v.y(),
            b: v.z(),
        }
    }

    pub fn to_vec(&self) -> Vec3 {
        Vec3::create(self.r, self.g, self.b)
    }

    pub fn as_ppm_norm(&self, samples_per_pixel: u32) -> String {
        let scale = 1.0 / (samples_per_pixel as f32);

        let r = (self.r * scale).sqrt();
        let g = (self.g * scale).sqrt();
        let b = (self.b * scale).sqrt();

        format!(
            "{} {} {}",
            (256.0 * clamp(r, 0.0, 0.99999)) as u32,
            (256.0 * clamp(g, 0.0, 0.99999)) as u32,
            (256.0 * clamp(b, 0.0, 0.99999)) as u32,
        )
    }

    pub fn as_ppm(&self) -> String {
        format!(
            "{} {} {}",
            (256.0 * clamp(self.r, 0.0, 0.99999)) as u32,
            (256.0 * clamp(self.g, 0.0, 0.99999)) as u32,
            (256.0 * clamp(self.b, 0.0, 0.99999)) as u32,
        )
    }

    pub fn random() -> Self {
        let mut rng = rand::thread_rng();
        Self {
            r: rng.gen_range(0.0..1.0),
            g: rng.gen_range(0.0..1.0),
            b: rng.gen_range(0.0..1.0),
        }
    }

    pub fn max(&self) -> f32 {
        self.r.max(self.g).max(self.b)
    }

    pub fn min(&self) -> f32 {
        self.r.min(self.g).min(self.b)
    }

    fn which_max(&self) -> ColorComponent {
        if self.r > self.g && self.r > self.b {
            ColorComponent::Red
        } else if self.g > self.r && self.g > self.b {
            ColorComponent::Green
        } else {
            ColorComponent::Blue
        }
    }

    pub fn dim(&self, factor: f32) -> Self {
        let min = self.min();
        let max = self.max();
        let max_comp = self.which_max();

        let hp = match max_comp {
            ColorComponent::Red => (self.g - self.b) / (max - min),
            ColorComponent::Green => 2.0 + (self.b - self.r) / (max - min),
            ColorComponent::Blue => 4.0 + (self.r - self.g) / (max - min),
        };

        let h = 60.0 * hp;
        let s = if max == min { 0.0 } else { (max - min) / max };
        let v = max;

        let v = v * factor;

        let hi = (h / 60.0).floor() as i32;
        let f = h / 60.0 - hi as f32;
        let p = v * (1.0 - s);
        let q = v * (1.0 - f * s);
        let t = v * (1.0 - s * (1.0 - f));

        let (r, g, b) = match hi {
            1 => (q, v, p),
            2 => (p, v, t),
            3 => (p, q, v),
            4 => (t, p, v),
            5 => (v, p, q),
            _ => (v, t, p),
        };

        Color { r, g, b }
    }

    pub fn sqrt(&self) -> Self {
        Color {
            r: self.r.sqrt(),
            g: self.g.sqrt(),
            b: self.b.sqrt(),
        }
    }
}

impl ops::Add for Color {
    type Output = Color;

    fn add(self, other: Color) -> Color {
        Color {
            r: self.r + other.r,
            g: self.g + other.g,
            b: self.b + other.b,
        }
    }
}

impl ops::AddAssign for Color {
    fn add_assign(&mut self, other: Self) {
        *self = Color {
            r: self.r + other.r,
            g: self.g + other.g,
            b: self.b + other.b,
        }
    }
}
impl ops::Mul<Color> for f32 {
    type Output = Color;

    fn mul(self, other: Color) -> Color {
        Color {
            r: self * other.r,
            g: self * other.g,
            b: self * other.b,
        }
    }
}

impl ops::Mul for Color {
    type Output = Color;

    fn mul(self, other: Color) -> Color {
        Color {
            r: self.r * other.r,
            g: self.g * other.g,
            b: self.b * other.b,
        }
    }
}
