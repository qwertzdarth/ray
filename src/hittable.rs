use std::option::Option;
use std::sync::Arc;

use crate::*;

pub struct HitRecord {
    pub p: Vec3,
    pub normal: Vec3,
    pub t: f32,
    pub front_face: bool,
    pub material: Arc<dyn Material + Send + Sync>,
}

impl HitRecord {
    pub fn create_with_outward_normal(
        r: Ray,
        p: Vec3,
        outward_normal: Vec3,
        t: f32,
        material: Arc<dyn Material + Send + Sync>,
    ) -> HitRecord {
        let front_face = r.direction.dot(outward_normal) < 0.0;
        HitRecord {
            p,
            normal: if front_face {
                outward_normal
            } else {
                -outward_normal
            },
            t,
            front_face,
            material,
        }
    }
}

pub trait Hittable {
    fn hit(&self, r: Ray, t_min: f32, t_max: f32) -> Option<HitRecord>;
    fn bounding_box(&self) -> Option<AABB>;
}

pub struct HittableList {
    objects: Vec<Arc<dyn Hittable + Send + Sync>>,
}

impl HittableList {
    pub fn create() -> HittableList {
        HittableList {
            objects: Vec::new(),
        }
    }

    pub fn create_from_one(object: Arc<dyn Hittable + Send + Sync>) -> HittableList {
        HittableList {
            objects: vec![object],
        }
    }

    pub fn clear(&mut self) {
        self.objects.clear();
    }

    pub fn add(&mut self, object: Arc<dyn Hittable + Send + Sync>) {
        self.objects.push(object);
    }

    pub fn hit(self, r: Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
        let mut temp_record: Option<HitRecord> = None;
        let mut closest_so_far = t_max;

        for object in self.objects {
            if let Some(record) = object.hit(r, t_min, closest_so_far) {
                closest_so_far = record.t;
                temp_record = Some(record);
            };
        }

        temp_record
    }
}

impl Hittable for HittableList {
    fn hit(&self, r: Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
        let mut temp_record: Option<HitRecord> = None;
        let mut closest_so_far = t_max;

        for object in &self.objects {
            if let Some(record) = object.hit(r, t_min, closest_so_far) {
                closest_so_far = record.t;
                temp_record = Some(record);
            };
        }

        temp_record
    }

    fn bounding_box(&self) -> Option<AABB> {
        if self.objects.is_empty() {
            None
        } else {
            let mut temp: Option<AABB> = None;

            for object in &self.objects {
                if let Some(obox) = object.bounding_box() {
                    temp = match temp {
                        Some(ref tbox) => Some(obox.combine_with(tbox)),
                        None => Some(obox),
                    };
                };
            }

            temp
        }
    }
}
