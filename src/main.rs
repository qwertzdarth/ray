use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::LineWriter;
use std::path::Path;

use clap::Parser;
use ray::random_scene_bvh;
use ray::render;
use ray::render_par;
use ray::Canvas;
use ray::Color;
use ray::RenderConfig;
use ray::RenderInput;
use ray::Vec3;

use std::convert::TryInto;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    #[arg(short = 'j', long, default_value = "1")]
    pub parallelism: usize,

    #[arg(long, default_value = "2560")]
    pub width: usize,

    #[arg(long, default_value = "1440")]
    pub height: usize,

    #[arg(long, default_value = "30")]
    pub samples_per_pixel: usize,

    #[arg(long, default_value = "10")]
    pub max_depth: usize,
}

fn main() {
    let args = Args::parse();

    let config = RenderConfig {
        image_width: args.width.try_into().unwrap(),
        image_height: args.height.try_into().unwrap(),
        samples_per_pixel: args.samples_per_pixel.try_into().unwrap(),
        max_depth: args.max_depth.try_into().unwrap(),
        background: Color::BLACK,

        look_from: Vec3::create(13.0, 2.0, 3.0),
        look_to: Vec3::create(0.0, 0.0, 0.0),
        look_vup: Vec3::up(),
        vfov: 40.0,
        aperture: 0.001,
        dist_to_focus: 10.0,
    };

    let world = random_scene_bvh();

    let path = Path::new("image.ppm");

    let file = File::create(path).expect("couldn't write to file");
    let mut out = LineWriter::new(file);

    let input = RenderInput {
        world,
        config,
        on_progress: |i, total, _, _| {
            let pct = (i as f32 / total as f32) * 100.0;

            print!("\r");
            print!("rendering {:4.1}%", pct);

            io::stdout().flush().unwrap();
        },
    };

    let canvas = Canvas::new(config.image_width, config.image_height);
    if args.parallelism > 1 {
        render_par(&canvas, &input, args.parallelism);
    } else {
        render(&canvas, &input);
    }

    println!();

    canvas.write_ppm(&mut out).expect("couldn't write to file");

    println!("rendered image to \"{}\"", path.display());
}
