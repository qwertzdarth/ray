use crate::*;
use std::cmp::Ordering;

#[derive(Debug, Copy, Clone)]
pub struct AABB {
    pub minimum: Vec3,
    pub maximum: Vec3,
}

impl AABB {
    pub fn create(min: Vec3, max: Vec3) -> AABB {
        AABB {
            minimum: min,
            maximum: max,
        }
    }

    pub fn combine_with(&self, other: &AABB) -> AABB {
        let minimum = Vec3::create(
            self.minimum.x().min(other.minimum.x()),
            self.minimum.y().min(other.minimum.y()),
            self.minimum.z().min(other.minimum.z()),
        );
        let maximum = Vec3::create(
            self.maximum.x().max(other.maximum.x()),
            self.maximum.y().max(other.maximum.y()),
            self.maximum.z().max(other.maximum.z()),
        );

        AABB { minimum, maximum }
    }

    pub fn hit(&self, ray: Ray, t_min: f32, t_max: f32) -> bool {
        let xt0a = (self.minimum.x() - ray.origin.x()) / ray.direction.x();
        let xt0b = (self.maximum.x() - ray.origin.x()) / ray.direction.x();

        let xt0 = xt0a.min(xt0b);
        let xt1 = xt0a.max(xt0b);

        let xt_min = xt0.max(t_min);
        let xt_max = xt1.min(t_max);

        let yt0a = (self.minimum.y() - ray.origin.y()) / ray.direction.y();
        let yt0b = (self.maximum.y() - ray.origin.y()) / ray.direction.y();

        let yt0 = yt0a.min(yt0b);
        let yt1 = yt0a.max(yt0b);

        let yt_min = yt0.max(t_min);
        let yt_max = yt1.min(t_max);

        let zt0a = (self.minimum.z() - ray.origin.z()) / ray.direction.z();
        let zt0b = (self.maximum.z() - ray.origin.z()) / ray.direction.z();

        let zt0 = zt0a.min(zt0b);
        let zt1 = zt0a.max(zt0b);

        let zt_min = zt0.max(t_min);
        let zt_max = zt1.min(t_max);

        !(xt_max <= xt_min || yt_max <= yt_min || zt_max <= zt_min)
    }

    pub fn compare_axis(s: &AABB, other: &AABB, axis: u32) -> Ordering {
        if axis == 0 {
            s.minimum.x().partial_cmp(&other.minimum.x()).unwrap()
        } else if axis == 1 {
            s.minimum.y().partial_cmp(&other.minimum.y()).unwrap()
        } else {
            s.minimum.z().partial_cmp(&other.minimum.z()).unwrap()
        }
    }

    pub fn compare_x_axis(s: &AABB, other: &AABB) -> Ordering {
        AABB::compare_axis(s, other, 0)
    }

    pub fn compare_y_axis(s: &AABB, other: &AABB) -> Ordering {
        AABB::compare_axis(s, other, 1)
    }

    pub fn compare_z_axis(s: &AABB, other: &AABB) -> Ordering {
        AABB::compare_axis(s, other, 2)
    }
}
