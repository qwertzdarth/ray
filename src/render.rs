use std::sync::atomic::AtomicU32;
use std::sync::atomic::Ordering;
use std::time::Duration;
use std::time::Instant;

use rand::*;
use rand::seq::SliceRandom;

use crate::*;

use rayon::iter::ParallelIterator;
use rayon::prelude::IntoParallelIterator;

#[derive(Debug, Clone, Copy)]
pub struct RenderConfig {
    pub image_width: u32,
    pub image_height: u32,
    pub samples_per_pixel: u32,
    pub max_depth: u32,
    pub background: Color,

    pub look_from: Vec3,
    pub look_to: Vec3,
    pub look_vup: Vec3,
    pub vfov: f32,
    pub aperture: f32,
    pub dist_to_focus: f32,
}

impl RenderConfig {
    pub fn make_camera(&self) -> Camera {
        Camera::create(
            self.look_from,
            self.look_to,
            self.look_vup,
            self.vfov,
            self.image_width as f32 / self.image_height as f32,
            self.aperture,
            self.dist_to_focus,
        )
    }
}

pub struct RenderInput {
    pub world: BVHNode,
    pub config: RenderConfig,

    pub on_progress: fn(u32, u32, Duration, Duration),
}

pub fn ray_color(r: Ray, background: Color, world: &dyn Hittable, depth: u32) -> Color {
    if depth == 0 {
        // Color::BLACK
        background
    } else {
        match world.hit(r, 0.001, f32::INFINITY) {
            Some(hit) => {
                let emitted = hit.material.emitted();
                match hit.material.scatter(&r, &hit) {
                    Some(scatter_info) => {
                        emitted
                            + scatter_info.attenuation
                                * ray_color(scatter_info.scattered, background, world, depth - 1)
                    }
                    None => emitted,
                }
            }
            None => {
                // background
                let unit_direction = r.direction.normalized();
                let t = 0.5 * (unit_direction.y() + 1.0);

                ((1.0 - t) * Color::WHITE + t * Color::SKY_BLUE).dim(0.6)
            }
        }
    }
}

pub fn render_par(canvas: &Canvas, input: &RenderInput, parallelism: usize) {
    let pool = rayon::ThreadPoolBuilder::new()
        .num_threads(parallelism)
        .build()
        .unwrap();

    pool.install(|| {
        let start_time = Instant::now();

        let rc = &input.config;
        let camera = rc.make_camera();

        let progress = AtomicU32::new(0);

        let mut idxs = (0..rc.image_height).rev().collect::<Vec<u32>>();
        idxs.shuffle(&mut rand::thread_rng());

        idxs.into_par_iter().for_each(|j| {
            let now = Instant::now();

            let pixels: Vec<Color> = (0..rc.image_width)
                // .into_iter()
                .map(|i| {
                    let mut rng = rand::thread_rng();

                    let mut color_vec = Color::BLACK;
                    for _ in 0..rc.samples_per_pixel {
                        let u = (i as f32 + rng.gen_range(0.0..1.0)) / (rc.image_width - 1) as f32;
                        let v = (j as f32 + rng.gen_range(0.0..1.0)) / (rc.image_height - 1) as f32;

                        let ray = camera.get_ray(u, v);
                        color_vec += ray_color(ray, rc.background, &input.world, rc.max_depth);
                    }
                    color_vec
                })
                .collect();

            let mut line = Vec::new();

            for pixel in pixels {
                let factor = 1.0 / rc.samples_per_pixel as f32;
                line.push((factor * pixel).sqrt());
            }

            canvas.set_line(rc.image_height - j - 1, line);

            progress.fetch_add(1, Ordering::SeqCst);
            (input.on_progress)(
                progress.load(Ordering::SeqCst),
                rc.image_height,
                now.elapsed(),
                start_time.elapsed(),
            );
        });
    });
}

pub fn render(canvas: &Canvas, input: &RenderInput) {
    let start_time = Instant::now();

    let rc = &input.config;
    let camera = rc.make_camera();

    let mut progress = 0;

    let mut idxs = (0..rc.image_height).rev().collect::<Vec<u32>>();
    idxs.shuffle(&mut rand::thread_rng());

    for j in idxs {
        let now = Instant::now();

        let pixels: Vec<Color> = (0..rc.image_width)
            .map(|i| {
                let mut rng = rand::thread_rng();

                let mut color_vec = Color::BLACK;
                for _ in 0..rc.samples_per_pixel {
                    let u = (i as f32 + rng.gen_range(0.0..1.0)) / (rc.image_width - 1) as f32;
                    let v = (j as f32 + rng.gen_range(0.0..1.0)) / (rc.image_height - 1) as f32;

                    let ray = camera.get_ray(u, v);
                    color_vec += ray_color(ray, rc.background, &input.world, rc.max_depth);
                }
                color_vec
            })
            .collect();

        let mut line = Vec::new();

        for pixel in pixels {
            let factor = 1.0 / rc.samples_per_pixel as f32;
            line.push((factor * pixel).sqrt());
        }

        canvas.set_line(rc.image_height - j - 1, line);

        progress += 1;
        (input.on_progress)(
            progress,
            rc.image_height,
            now.elapsed(),
            start_time.elapsed(),
        );
    }
}
