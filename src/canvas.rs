use std::sync::Mutex;

use crate::*;

pub struct Canvas {
    pub width: u32,
    pub height: u32,

    pixels: Mutex<Vec<Color>>,
}

impl Canvas {
    pub fn new(width: u32, height: u32) -> Canvas {
        Canvas {
            width,
            height,
            pixels: Mutex::new(vec![Color::BLACK; (width * height) as usize]),
        }
    }

    pub fn get_pixel(&self, x: u32, y: u32) -> Color {
        let pixels = self.pixels.lock().unwrap();
        pixels[(y * self.width + x) as usize]
    }

    pub fn set_pixel(&self, x: u32, y: u32, color: Color) {
        let mut pixels = self.pixels.lock().unwrap();
        pixels[(y * self.width + x) as usize] = color;
    }

    pub fn set_line(&self, y: u32, line: Vec<Color>) {
        let mut pixels = self.pixels.lock().unwrap();
        for (x, color) in line.into_iter().enumerate() {
            pixels[(y * self.width + x as u32) as usize] = color;
        }
    }

    pub fn write_ppm<W: std::io::Write>(&self, out: &mut W) -> Result<(), std::io::Error> {
        writeln!(out, "P3")?;
        writeln!(out, "{} {}", self.width, self.height)?;
        writeln!(out, "255")?;

        for y in 0..self.height {
            for x in 0..self.width {
                let color = self.get_pixel(x, y);
                let color = color.as_ppm_norm(1);
                write!(out, "{} ", color)?;
            }
            writeln!(out)?;
        }

        Ok(())
    }
}
