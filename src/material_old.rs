use std::option::Option;

use crate::hittable::HitRecord;
use crate::ray::Ray;
use crate::color::Color;
use crate::vec3::Vec3;

pub struct ScatterInfo {
    pub attenuation: Color,
    pub scattered: Ray,
}

pub trait Material {
    fn scatter(
        &self,
        ray: &Ray,
        hit_record: &HitRecord,
    ) -> Option<ScatterInfo>;
}

pub struct Diffuse {
    pub albedo: Color,
}

impl Diffuse {
    pub fn create(albedo: Color) -> Diffuse {
        Diffuse { albedo: albedo }
    }
}

impl Material for Diffuse {
    fn scatter(
        &self,
        _ray: &Ray,
        hit_record: &HitRecord,
    ) -> Option<ScatterInfo> {
        let mut scatter_direction = hit_record.normal + Vec3::random_on_unit_sphere();
        
        if scatter_direction.near_zero() {
            scatter_direction = hit_record.normal;
        }

        Some(ScatterInfo {
            attenuation: self.albedo,
            scattered: Ray { origin: hit_record.p, direction: scatter_direction },
        })
    }
}

pub struct Metal {
    pub albedo: Color,
    pub fuzz: f32,
}

impl Metal {
    pub fn create(albedo: Color, fuzz: f32) -> Metal {
        Metal { 
            albedo: albedo,
            fuzz: if fuzz <= 1.0 { fuzz } else { 1.0 }
        }
    }
}

impl Material for Metal {
    fn scatter(
        &self,
        ray: &Ray,
        hit_record: &HitRecord,
    ) -> Option<ScatterInfo> {
        let reflected = Vec3::reflect(ray.direction.normalized(), hit_record.normal);

        if reflected.dot(hit_record.normal) > 0.0 {
            Some(ScatterInfo {
                attenuation: self.albedo,
                scattered: Ray { origin: hit_record.p, direction: reflected + self.fuzz * Vec3::random_in_unit_sphere() }
            })
        } else {
            None
        }
    }
}
