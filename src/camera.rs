use crate::ray::Ray;
use crate::vec3::Vec3;

use std::f32::consts::PI;

pub struct Camera {
    origin: Vec3,
    lower_left_corner: Vec3,

    horizontal: Vec3,
    vertical: Vec3,

    u: Vec3,
    v: Vec3,

    #[allow(dead_code)]
    w: Vec3,

    lens_radius: f32,
}

impl Camera {
    pub fn create(
        from: Vec3,
        to: Vec3,
        vup: Vec3,

        vfov: f32,
        aspect_ratio: f32,

        aperture: f32,
        dist_to_focus: f32,
    ) -> Camera {
        let theta = (vfov / 360.0) * 2.0 * PI;
        let h = (theta / 2.0).tan();
        let viewport_height = 2.0 * h;
        let viewport_width = aspect_ratio * viewport_height;

        let w = (from - to).normalized();
        let u = vup.cross(w).normalized();
        let v = w.cross(u);

        let horizontal = dist_to_focus * viewport_width * u;
        let vertical = dist_to_focus * viewport_height * v;

        Camera {
            origin: from,
            lower_left_corner: from - horizontal / 2.0 - vertical / 2.0 - w * dist_to_focus,

            horizontal,
            vertical,

            u,
            v,
            w,

            lens_radius: aperture / 2.0,
        }
    }

    pub fn get_ray(&self, s: f32, t: f32) -> Ray {
        let rd = self.lens_radius * Vec3::random_in_unit_disk();
        let offset = self.u * rd.x() + self.v * rd.y();

        Ray {
            origin: self.origin + offset,
            direction: self.lower_left_corner + s * self.horizontal + t * self.vertical
                - self.origin
                - offset,
        }
    }
}
