use rand::Rng;
use std::ops;

#[derive(Debug, Copy, Clone)]
pub struct Vec3([f32; 4]);

impl Vec3 {
    #[inline]
    pub fn x(&self) -> f32 {
        self.0[0]
    }

    #[inline]
    pub fn y(&self) -> f32 {
        self.0[1]
    }

    #[inline]
    pub fn z(&self) -> f32 {
        self.0[2]
    }

    pub fn create(x: f32, y: f32, z: f32) -> Self {
        Self([x, y, z, 0.0])
    }

    pub fn origin() -> Self {
        Self([0.0; 4])
    }

    pub fn unit() -> Self {
        Self([1.0, 1.0, 1.0, 0.0])
    }

    pub fn up() -> Self {
        Self([0.0, 1.0, 0.0, 0.0])
    }

    pub fn random() -> Self {
        let mut rng = rand::thread_rng();
        Self::create(
            rng.gen_range(0.0..1.0),
            rng.gen_range(0.0..1.0),
            rng.gen_range(0.0..1.0),
        )
    }

    pub fn random_range(range: ops::Range<f32>) -> Self {
        let mut rng = rand::thread_rng();
        Self::create(
            rng.gen_range(range.start..range.end),
            rng.gen_range(range.start..range.end),
            rng.gen_range(range.start..range.end),
        )
    }

    pub fn random_in_unit_sphere() -> Self {
        loop {
            let v = Self::random_range(-1.0..1.0);
            if v.length_squared() < 1.0 {
                return v;
            }
        }
    }

    pub fn random_on_unit_sphere() -> Self {
        Self::random_in_unit_sphere().normalized()
    }

    pub fn random_in_unit_disk() -> Self {
        let mut rng = rand::thread_rng();
        loop {
            let v = Self::create(rng.gen_range(-1.0..1.0), rng.gen_range(-1.0..1.0), 0.0);

            if v.length_squared() <= 1.0 {
                return v;
            }
        }
    }

    pub fn sum(&self) -> f32 {
        let mut r = 0.0;

        for i in 0..4 {
            r += self.0[i];
        }
        r
    }

    pub fn dot(&self, other: Vec3) -> f32 {
        let mut r = 0.0;
        for i in 0..3 {
            r += self.0[i] * other.0[i];
        }
        r
    }

    pub fn cross(self, other: Self) -> Self {
        Self::create(
            self.y() * other.z() - self.z() * other.y(),
            self.z() * other.x() - self.x() * other.z(),
            self.x() * other.y() - self.y() * other.x(),
        )
    }

    pub fn length(self) -> f32 {
        self.length_squared().sqrt()
    }

    pub fn length_squared(self) -> f32 {
        (self * self).sum()
    }

    pub fn normalized(self) -> Self {
        self / self.length()
    }

    pub fn near_zero(&self) -> bool {
        let s = 1e-8;
        self.x().abs() < s && self.y().abs() < s && self.z().abs() < s
    }
}

impl ops::Neg for Vec3 {
    type Output = Vec3;

    fn neg(self) -> Vec3 {
        Self(self.0.map(|c| -c))
    }
}

impl ops::Add for Vec3 {
    type Output = Vec3;

    fn add(self, other: Vec3) -> Vec3 {
        let mut v: [f32; 4] = [0.0; 4];
        for (i, c) in self.0.iter().enumerate() {
            v[i] = c + other.0[i];
        }
        Self(v)
    }
}

impl ops::Sub for Vec3 {
    type Output = Vec3;

    fn sub(self, other: Vec3) -> Vec3 {
        let mut v: [f32; 4] = [0.0; 4];
        for (i, c) in self.0.iter().enumerate() {
            v[i] = c - other.0[i];
        }
        Self(v)
    }
}

impl ops::Mul<f32> for Vec3 {
    type Output = Vec3;

    fn mul(self, other: f32) -> Vec3 {
        Vec3(self.0.map(|c| c * other))
    }
}

impl ops::Mul<Vec3> for f32 {
    type Output = Vec3;

    fn mul(self, other: Vec3) -> Vec3 {
        let mut v: [f32; 4] = [0.0; 4];
        for (i, c) in other.0.iter().enumerate() {
            v[i] = self * c;
        }
        Vec3(v)
    }
}

impl ops::Mul for Vec3 {
    type Output = Vec3;

    fn mul(self, other: Vec3) -> Vec3 {
        let mut v: [f32; 4] = [0.0; 4];
        for (i, c) in self.0.iter().enumerate() {
            v[i] = c * other.0[i];
        }
        Self(v)
    }
}

impl ops::Div<f32> for Vec3 {
    type Output = Vec3;

    fn div(self, other: f32) -> Vec3 {
        self * (1.0f32 / other)
    }
}
