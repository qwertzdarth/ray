use std::sync::Arc;

use rand::*;

use crate::*;

#[allow(dead_code)]
pub fn random_scene() -> HittableList {
    let mut rng = rand::thread_rng();

    let mut world = HittableList::create();

    let ground_material = Arc::new(Diffuse::create(Color::create(0.5, 0.5, 0.5)));
    world.add(Arc::new(Sphere {
        center: Vec3::create(0.0, -1000.0, 0.0),
        radius: 1000.0,
        material: ground_material.clone(),
    }));

    for a in -11..11 {
        for b in -11..11 {
            let choose_mat: f32 = rng.gen();

            let ar: f32 = rng.gen();
            let br: f32 = rng.gen();

            let x: f32 = (a as f32) + 0.9 * ar;
            let z: f32 = (b as f32) + 0.9 * br;

            let center = Vec3::create(x, 0.2, z);

            if (center - Vec3::create(4.0, 0.2, 0.0)).length() > 0.9 {
                if choose_mat < 0.8 {
                    // diffuse
                    let mat = Arc::new(Diffuse::create(Color::random() * Color::random()));
                    world.add(Arc::new(Sphere {
                        center,
                        radius: 0.2,
                        material: mat,
                    }));
                } else if choose_mat < 0.95 {
                    // metal
                    let mat = Arc::new(Metal::create(
                        0.5 * Color::random() + 0.5 * Color::WHITE,
                        rng.gen_range(0.0f32..1.0f32),
                    ));
                    world.add(Arc::new(Sphere {
                        center,
                        radius: 0.2,
                        material: mat,
                    }));
                } else {
                    // glass
                    let mat = Arc::new(Dielectric::create(1.5));
                    world.add(Arc::new(Sphere {
                        center,
                        radius: 0.2,
                        material: mat,
                    }));
                }
            }
        }
    }

    let mat1 = Arc::new(Dielectric::create(1.5));
    world.add(Arc::new(Sphere {
        center: Vec3::create(0.0, 1.0, 0.0),
        radius: 1.0,
        material: mat1,
    }));

    let mat2 = Arc::new(Diffuse::create(Color::create(0.4, 0.2, 0.1)));
    world.add(Arc::new(Sphere {
        center: Vec3::create(-4.0, 1.0, 0.0),
        radius: 1.0,
        material: mat2,
    }));

    let mat3 = Arc::new(Metal::create(Color::create(0.7, 0.6, 0.5), 0.05));
    world.add(Arc::new(Sphere {
        center: Vec3::create(4.0, 1.0, 0.0),
        radius: 1.0,
        material: mat3,
    }));

    world
}

#[allow(dead_code)]
pub fn random_scene_bvh() -> BVHNode {
    let mut rng = rand::thread_rng();

    let mut world: Vec<Arc<dyn Hittable + Sync + Send>> = Vec::new();

    let ground_material = Arc::new(Diffuse::create(Color::create(0.5, 0.5, 0.5)));
    world.push(Arc::new(Sphere {
        center: Vec3::create(0.0, -1000.0, 0.0),
        radius: 1000.0,
        material: ground_material.clone(),
    }));

    for a in -11..11 {
        for b in -11..11 {
            let choose_mat: f32 = rng.gen();

            let ar: f32 = rng.gen();
            let br: f32 = rng.gen();

            let x: f32 = (a as f32) + 0.9 * ar;
            let z: f32 = (b as f32) + 0.9 * br;

            let center = Vec3::create(x, 0.2, z);

            if (center - Vec3::create(4.0, 0.2, 0.0)).length() > 0.9 {
                if choose_mat < 0.65 {
                    // diffuse
                    let mat = Arc::new(Diffuse::create(Color::random() * Color::random()));
                    world.push(Arc::new(Sphere {
                        center,
                        radius: 0.2,
                        material: mat,
                    }));
                } else if choose_mat < 0.85 {
                    // metal
                    let mat = Arc::new(Metal::create(
                        0.5 * Color::random() + 0.5 * Color::WHITE,
                        rng.gen_range(0.0f32..1.0f32),
                    ));
                    world.push(Arc::new(Sphere {
                        center,
                        radius: 0.2,
                        material: mat,
                    }));
                } else if choose_mat < 0.95 {
                    // glass
                    let mat = Arc::new(Dielectric::create(1.5));
                    world.push(Arc::new(Sphere {
                        center,
                        radius: 0.2,
                        material: mat,
                    }));
                } else {
                    // diffuse light
                    let mat = Arc::new(DiffuseLight::create(
                        0.5 * Color::WHITE + 0.5 * Color::random(),
                    ));
                    world.push(Arc::new(Sphere {
                        center,
                        radius: 0.3,
                        material: mat,
                    }));
                }
            }
        }
    }

    let mat1 = Arc::new(Dielectric::create(1.5));
    world.push(Arc::new(Sphere {
        center: Vec3::create(0.0, 1.0, 0.0),
        radius: 1.0,
        material: mat1,
    }));

    let mat2 = Arc::new(Diffuse::create(Color::create(0.4, 0.2, 0.1)));
    world.push(Arc::new(Sphere {
        center: Vec3::create(-4.0, 1.0, 0.0),
        radius: 1.0,
        material: mat2,
    }));

    let mat3 = Arc::new(Metal::create(Color::create(0.7, 0.6, 0.5), 0.05));
    world.push(Arc::new(Sphere {
        center: Vec3::create(4.0, 1.0, 0.0),
        radius: 1.0,
        material: mat3,
    }));

    let mat4 = Arc::new(DiffuseLight::create(0.8 * Color::WHITE));
    world.push(Arc::new(Sphere {
        center: Vec3::create(0.0, 13.0, 0.0),
        radius: 6.0,
        material: mat4,
    }));

    BVHNode::create(&mut world[..])
}

#[allow(dead_code)]
pub fn random_cube() -> HittableList {
    let mut rng = rand::thread_rng();

    let mut world = HittableList::create();

    let ground_material = Arc::new(Diffuse::create(Color::create(0.5, 0.5, 0.5)));
    world.add(Arc::new(Sphere {
        center: Vec3::create(0.0, -100000.0, 0.0),
        radius: 100000.0,
        material: ground_material.clone(),
    }));

    for i in 0..10 {
        for j in 0..10 {
            for k in 0..10 {
                let xr: f32 = rng.gen();
                let yr: f32 = rng.gen();
                let zr: f32 = rng.gen();

                let x = (i as f32) + 0.9 * xr;
                let y = (k as f32) + 0.9 * yr;
                let z = (j as f32) + 0.9 * zr;

                let center = Vec3::create(x, y, z);

                let choose_mat: f32 = rng.gen();

                if choose_mat < 0.6 {
                    // diffuse
                    let mat = Arc::new(Diffuse::create(Color::random() * Color::random()));
                    world.add(Arc::new(Sphere {
                        center,
                        radius: 0.5,
                        material: mat,
                    }));
                } else if choose_mat < 0.8 {
                    // metal
                    let mat = Arc::new(Metal::create(
                        0.5 * Color::random() + 0.5 * Color::WHITE,
                        rng.gen_range(0.0f32..1.0f32),
                    ));
                    world.add(Arc::new(Sphere {
                        center,
                        radius: 0.5,
                        material: mat,
                    }));
                } else {
                    // glass
                    let mat = Arc::new(Dielectric::create(1.5));
                    world.add(Arc::new(Sphere {
                        center,
                        radius: 0.5,
                        material: mat,
                    }));
                }
            }
        }
    }

    // BVHNode::create(&mut world[0..])
    world
}

#[allow(dead_code)]
pub fn random_cube_bvh() -> BVHNode {
    let mut rng = rand::thread_rng();

    let mut world: Vec<Arc<dyn Hittable + Sync + Send>> = Vec::new();

    let ground_material = Arc::new(Diffuse::create(Color::create(0.5, 0.5, 0.5)));
    world.push(Arc::new(Sphere {
        center: Vec3::create(0.0, -100000.0, 0.0),
        radius: 100000.0,
        material: ground_material.clone(),
    }));

    for i in 0..10 {
        for j in 0..10 {
            for k in 0..10 {
                let xr: f32 = rng.gen();
                let yr: f32 = rng.gen();
                let zr: f32 = rng.gen();

                let x = (i as f32) + 0.9 * xr;
                let y = (k as f32) + 0.9 * yr;
                let z = (j as f32) + 0.9 * zr;

                let center = Vec3::create(x, y, z);

                let choose_mat: f32 = rng.gen();

                if choose_mat < 0.6 {
                    // diffuse
                    let mat = Arc::new(Diffuse::create(Color::random() * Color::random()));
                    world.push(Arc::new(Sphere {
                        center,
                        radius: 0.5,
                        material: mat,
                    }));
                } else if choose_mat < 0.8 {
                    // metal
                    let mat = Arc::new(Metal::create(
                        0.5 * Color::random() + 0.5 * Color::WHITE,
                        rng.gen_range(0.0f32..1.0f32),
                    ));
                    world.push(Arc::new(Sphere {
                        center,
                        radius: 0.5,
                        material: mat,
                    }));
                } else if choose_mat < 0.98 {
                    // glass
                    let mat = Arc::new(Dielectric::create(1.5));
                    world.push(Arc::new(Sphere {
                        center,
                        radius: 0.5,
                        material: mat,
                    }));
                } else {
                    // diffuse light
                    let mat = Arc::new(DiffuseLight::create(Color::random() * Color::random()));
                    world.push(Arc::new(Sphere {
                        center,
                        radius: 0.3,
                        material: mat,
                    }));
                }
            }
        }
    }

    BVHNode::create(&mut world[0..])
}
