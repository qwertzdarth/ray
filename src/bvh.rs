use crate::*;

use std::sync::Arc;
use std::fmt;
use rand::Rng;

use std::cmp::Ordering::*;

pub enum BVHNode {
    Inner {
	left: Arc<BVHNode>,
	right: Arc<BVHNode>,
	bounds: AABB,
    },
    Leaf(Arc<dyn Hittable + Sync + Send>),
}

use BVHNode::*;

impl Hittable for BVHNode {
    fn hit(&self, r: Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
	match self {
	    Leaf(hittable) => {
		hittable.hit(r, t_min, t_max)
	    },
	    Inner { left, right, bounds } => {
		if !bounds.hit(r, t_min, t_max) {
		    None
		} else {
		    let hit_left = left.hit(r, t_min, t_max);
		    match hit_left {
			None => right.hit(r, t_min, t_max),
			Some(ref hrl) => {
			    right.hit(r, t_min, hrl.t).or(hit_left)
			}
		    }
		}
	    }
	}
    }

    fn bounding_box(&self) -> Option<AABB> {
	match self {
	    Leaf(hittable) => hittable.bounding_box(),
	    Inner { bounds, .. } => Some(*bounds)
	}
    }
}

impl fmt::Debug for BVHNode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
	match self {
	    Leaf(_) => f.debug_struct("Leaf").finish(),
	    Inner {
		left,
		right,
		..
	    } => f.debug_struct("Inner").field("left", left).field("right", right).finish(),
	}
    }
}

impl BVHNode {
    pub fn create(src_objects: &mut[Arc<dyn Hittable + Send + Sync>]) -> BVHNode {
	let mut rng = rand::thread_rng();

	let axis = rng.gen_range(0..3); 
	let comparator = if axis == 0 {
	    AABB::compare_x_axis
	} else if axis == 1 {
	    AABB::compare_y_axis
	} else {
	    AABB::compare_z_axis
	};

	if src_objects.len() == 1 {
	    Leaf(src_objects[0].clone())
	} else if src_objects.len() == 2 {
	    let left_candidate = src_objects[0].clone();
	    let right_candidate = src_objects[1].clone();

	    match left_candidate.bounding_box() {
		Some(lbb) => {
		    match right_candidate.bounding_box() {
			Some(rbb) => {
			    match comparator(&lbb, &rbb) {
				Less | Equal => {
				    Inner {
					left: Arc::new(Leaf(left_candidate)),
					right: Arc::new(Leaf(right_candidate)),
					bounds: lbb.combine_with(&rbb),
				    }
				}
				Greater => {
				    Inner {
					right: Arc::new(Leaf(left_candidate)),
					left: Arc::new(Leaf(right_candidate)),
					bounds: lbb.combine_with(&rbb),
				    }
				}
			    }
			}
			None => Leaf(left_candidate.clone())
		    }
		}
		None => Leaf(right_candidate.clone())
	    }
	} else {
	    src_objects.sort_by(| a, b | {
		comparator(&a.bounding_box().unwrap(), &b.bounding_box().unwrap())
	    });

	    let mid = src_objects.len() / 2;

	    let left = BVHNode::create(&mut src_objects[0..mid]);
	    let right = BVHNode::create(&mut src_objects[mid..]);

	    let bounds = left.bounding_box().or(right.bounding_box()).unwrap().combine_with(&right.bounding_box().or(left.bounding_box()).unwrap());
	    
	    Inner {
		left: Arc::new(left), right: Arc::new(right), bounds 
	    }
	}
    }
}
